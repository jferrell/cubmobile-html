var touch_event = (typeof Touch == "object") && ('__proto__' in {}) ? 'tap' : 'click';
var scroll_event = (typeof Touch == "object") ? 'touchmove' : 'scroll';
var cubcadet = (typeof cubcadet !== 'undefined') ? cubcadet : {};
cubcadet.mobile = (typeof cubcadet.mobile !== 'undefined') ? cubcadet.mobile : {};

(function (ccm) {
	var duration = 500;	
	var isAndroidBrowser = (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Chrome') < 0) ? true : false;
	// one line below is fix for Android 2 browser
	// position: fixed causes select elements to be untappable
	// see CSS in HEADER section for more
	if (navigator.userAgent.indexOf('Android 2') > 0) document.documentElement.className += ' android-browser-two'; 

	ccm.optimize = {
		init : function () {
			$('.lazy-load').animate({ opacity:0 }, 0).each(ccm.optimize.lazyLoad);
			$(window).on(scroll_event, function () {
				// lazy load on scroll
				$('.lazy-load').each(ccm.optimize.lazyLoad);
			});
			$('#phone').on('keyup', function() {
				if ($(this).val().length === 3) {
					$('#phone2').focus();
				}
			});
			$('#phone2').on('keyup', function() {
				if ($(this).val().length === 3) {
					$('#phone3').focus();
				}
			});
		},
		lazyLoad : function () {
			var el = this;
			var src;
			// make sure image is in viewport
			if(ccm.optimize.elementInViewport(el) || $(el).hasClass('slider-img')) {
				// check for retina display
				if( window.devicePixelRatio >= 1.5 ){
					src = $(el).data('img-hd');
				} else {
					src = $(el).data('img');
				}
				var ajax_cache = true;
				// do not cache ajax request on Android browser due to bug
				if (isAndroidBrowser) ajax_cache = false;
				$.ajax({
					url: src,
					cache: ajax_cache,
					success: function(data) {
						$(el).attr('src', src).animate({ opacity: 1 }, 300).removeClass();
					}
				});	
			}
		},
		elementInViewport : function (el) {
		  var top = el.offsetTop;
		  var left = el.offsetLeft;
		  var width = el.offsetWidth;
		  var height = el.offsetHeight;
		  while(el.offsetParent) {
		    el = el.offsetParent;
		    top += el.offsetTop;
		    left += el.offsetLeft;
		  }
		  return (
		    top < (window.pageYOffset + window.innerHeight + 80) &&
		    left < (window.pageXOffset + window.innerWidth) &&
		    (top + height) > window.pageYOffset
		  );
		}
	}
	ccm.cart = {
		init : function() {
			$('.select-submit').on('change', ccm.cart.selectSubmit);
			$('#credit-card-number').on('keyup', ccm.cart.cardListener);	
			$('.form-checkbox-shipping').on('click', ccm.cart.billingSame);
		},
		expandItem : function(e) {
			if (! $(this).parent().hasClass('expanded')) {
				$('.cart li').removeClass('expanded');
			}
			$(this).parent().toggleClass('expanded');
		},
		billingSame : function(e) {
			// set checkbox if click from label
			var check = $('input', this);
			if (e.target.type != 'checkbox') { 
				if (check.prop('checked')) {
					check.prop('checked', false);
				}else {
					check.prop('checked', true)
				}
			}	
			var required_fields = $('#cart-billing [data-valid]');
			if (! check.prop('checked')) { // add required prop to fields with data-valid attribute if unchecked
				required_fields.attr('required', '');
			}else { // remove required prop to fields with data-valid attribute if checked
				required_fields.removeAttr('required');
			}
			// toggle billing fields
			$('#cart-billing').toggle();
			$('#cart-billing input').each(ccm.controls.placeholders);
		},
		selectSubmit : function() {
			this.parentNode.submit();
		},
		cardListener : function() {
			var accepted_cards = $('.accepted-cards').find('li');
			var existing_match = $('.match');
			var card_type = ccm.cart.detectCardType($(this).attr('value').toString());
			if (card_type) {
				if (! existing_match.hasClass(card_type)) {
					accepted_cards.removeClass('match').removeClass('no-match');
					accepted_cards.each(function(el, index, xui) {
						if ($(this).hasClass(card_type)) {
							$(this).toggleClass('match');
						}else {
							$(this).toggleClass('no-match');		
						}
					});					
				}
			} else {
				accepted_cards.removeClass('match').removeClass('no-match');
			}
		},
		detectCardType : function(card_number) {
			card_number = card_number.replace(/\D/g, '');
			var cards = ccm.acceptedCards;
			for (var ci in cards) {
				if (cards.hasOwnProperty(ci)) {
					var c = cards[ci];
				    for(var pi = 0, pl = c.prefixes.length; pi < pl; ++pi) {
				    	if(c.prefixes.hasOwnProperty(pi)) {
				        	var p = c.prefixes[pi];
				          	if (new RegExp('^' + p.toString()).test(card_number)) {
				            	return ci;
										}
				        }
				    }
				}
			}
			return false;
		}
	}
	ccm.acceptedCards = {
		'visa' : {
			prefixes : [4]
		},
		'mastercard' : {
			prefixes : [51, 52, 53, 54, 55]
		},
		'americanexpress' : {
			prefixes : [34, 37]
		},
		'discover' : {
			prefixes : [6011, 62, 64, 65]
		}	
	};
	ccm.validate = {
		init : function() {
			$('form.validate').on('submit', ccm.validate.perform);
			$('form.validate .phone-fields').each(ccm.validate.phoneFields);
			$('form.validate input').on('invalid', ccm.validate.catchInvalid);
			$('select.seed-input').on('change', ccm.validate.seedInputs);
		},
		phoneFields : function(i) {	
			var phone_field = $(this).find('.hidden-phone');
			var area_code = $(this).find('.field1');
			var prefix = $(this).find('.field2');
			var suffix = $(this).find('.field3');
			$(area_code).on('keyup',function (e) {
				$(phone_field).val($(area_code).val() + $(prefix).val() + $(suffix).val());
			});
			$(prefix).on('keyup',function (e) {
				$(phone_field).val($(area_code).val() + $(prefix).val() + $(suffix).val());
			});
			$(suffix).on('keyup',function (e) {
				$(phone_field).val($(area_code).val() + $(prefix).val() + $(suffix).val());
			});
		},
		perform : function(e) {	
			var fields = [];
			$(this).find('*').each(function () {
				ccm.validate.hideError(this);
				if (this.hasAttribute('required')) {
					fields.push(this);
				}
			});
			var errors = 0;
			$(fields).each(function(i) {
				if (! ccm.validate.hasValue(this)) {
					ccm.validate.showError(this);
					errors++;
				}else {
					ccm.validate.hideError(this);
				}
			});
			if (errors > 0 || (ccm.validate.hascheckValidity() && !this.checkValidity())) {
				if(ccm.validate.hascheckValidity()) {
					this.checkValidity();
				}
				$('.user-errors').addClass('show');
				e.preventDefault();
			}
			if ($('.user-pwd-match').size() > 0) {
				if (! ccm.validate.passwordMatch()) {
					$('.user-pwd-match').addClass('show');	
					e.preventDefault();				
				} else {
					$('.user-pwd-match').removeClass('show');
				}
			}
			if ($('.user-email-match').size() > 0) {
				if (! ccm.validate.emailMatch()) {
					$('.user-email-match').addClass('show');	
					e.preventDefault();				
				} else {
					$('.user-email-match').removeClass('show');	
				}
			}
			
		},
		hascheckValidity : function() {
		  return (typeof document.createElement( 'input' ).checkValidity === 'function');
		},
		passwordMatch : function() {
			return $('#password').attr('value').toString() == $('#password-confirm').attr('value').toString();
		},
		emailMatch : function() {
			return $('#email').attr('value').toString() == $('#email-confirm').attr('value').toString();
		},
		hasValue : function(el) {
			var type = el.nodeName.toLowerCase();
			switch(type) {
				case 'select':
					return el.options[el.selectedIndex].value != '';
					break;
				default:
					if ($(el).attr('type').toString() == 'checkbox') {
						return el.checked;
					}else {
						return $(el).attr('value').toString().length > 0;	
					}					
					break;
			}
		},
		catchInvalid : function (e) {
			var el = this;
			var field_type = $(el).attr('type');
			var msg = el.validationMessage;
			if(msg != 'value missing' && $(el).val() != '') {
				switch(field_type) {
					case 'color':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'date':
						msg = 'Please enter a valid ' + field_type;
						break;	
					case 'datetime':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'datetime-local':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'email':
						msg = 'Please enter a valid ' + field_type + ' (must contain an "@" and ".")';
						break;
					case 'month':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'number':
						msg = 'Please enter a valid ' + field_type;
						break;	
					case 'range':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'search':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'tel':
						msg = 'Please enter a valid ' + $($('label[for="' + $(el).attr('id') + '"]')).html().replace('*','').replace(':','');
						break;
					case 'hidden':
						msg = 'Please enter a valid ' + $($('label[for="' + $(el).attr('id') + '"]')).html().replace('*','').replace(':','');
						break;	
					case 'time':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'url':
						msg = 'Please enter a valid ' + field_type;
						break;
					case 'week':
						msg = 'Please enter a valid ' + field_type;
						break;				
					default:
						msg = 'Please enter a valid ' + field_type;
						break;
				}
				$('#' + $(el).attr('data-valid')).html(msg);
				ccm.validate.showError(el);
			}
		},
		showError : function(el) {
			var field_error = $(el).attr('data-valid');
			if (field_error) {
				$('#' + field_error).addClass('show');
			}		
		},
		hideError : function(el) {
			var field_error = $(el).attr('data-valid');
			if (field_error) {
				$('#' + field_error).removeClass('show');
			}		
		},
		seedInputs: function(e) {
			if($(e.target).attr('id') == 'color-select') {
				$(this).next('.color-indicator').empty()
					.append('<img src="' + $(e.target).find('option').not(function(){ return !this.selected }).data("color-image") + '" alt="color preview" />');
			}
			$($(e.target).data('seed-input')).val($(e.target).val());
		}
	}
	ccm.controls = {
		init : function() {
			$('.form-checkbox').on(touch_event, ccm.controls.checkbox);
			$('.form-unit').on('click', ccm.controls.focusField);
			$('.form-input').on('click', ccm.controls.focusField);
			$('.label-inline input').each(ccm.controls.placeholders);
		},
		checkbox : function(e) {
			var check = $('input', this);
			if (e.target.type != 'checkbox') { 
				if (check.prop('checked')) {
					check.prop('checked', false);
				}else {
					check.prop('checked', true)
				}
			}			
		},
		placeholders : function() {
			if(this.type != 'checkbox' && this.type != 'radio' && this.type != 'hidden') {
				if (this.hasAttribute('required')) {
					$(this).attr('placeholder','required')
				} else if (!this.hasAttribute('required') && !$(this).hasClass('field-phone')) {
					$(this).attr('placeholder','optional')
				}
			}
		},
		focusField : function(e) {
			var el_type = e.target.tagName.toLowerCase();
			if (el_type != 'input' && el_type != 'select' && el_type != 'textarea' && el_type != 'label' && $(this).find('.form-input').size() == 0) { 
				$(e.target).find('input').focus();
				$(e.target).find('select').focus();
				$(e.target).find('textarea').focus();
			}
		}	
	}
	ccm.util = {		
		init : function() {
			$('ul').on('click', '.expand', ccm.util.expand);
			$('.expand-nested').on('click', ccm.util.expandNested);
			$('.collapse-only').on('click', ccm.util.collapseOnly);
			$('.toggle').on('click', ccm.util.toggle);
			$('.show-hide').on('click', ccm.util.showHide);
			$('.stars span').each(ccm.util.stars);
			$('#full-go').on('click', ccm.util.setFullSiteCookie);
		},
		setFullSiteCookie : function(e) {
			e.preventDefault();
			document.cookie = 'CubMobile=mobile=no;path=/';
			window.location = '/';
		},
		showHide : function(e) {
			e.preventDefault();
			if( ! $(this).hasClass('open')) {
				$(this).addClass('open');
				$($(this).attr('href')).removeClass('hide');
			} else {
				$(this).removeClass('open');
				$($(this).attr('href')).addClass('hide');
			}
		},
		expand : function(e) {					
			e.preventDefault();
			var parent = $(this).parent();
			if ( ! parent.hasClass('expanded') && !parent.hasClass('locked'))
			{
				var last = $('.expanded');
				last.find('.expand-icon').removeClass('icon-minus').addClass('icon-plus');
				last.removeClass('expanded').addClass('collapsed');
				parent.removeClass('collapsed').addClass('expanded').find('.expand-icon').removeClass('icon-plus').addClass('icon-minus');	
				window.scroll(0, parseInt($(this).offset().top) - 70);
				$('.lazy-load').each(ccm.optimize.lazyLoad);
				$('.stars span').each(ccm.util.stars);
				ccm.optimize.headerOverlay();
			}
			else if ( !parent.hasClass('locked'))
			{	
				parent.removeClass('expanded').addClass('collapsed').find('.expand-icon').removeClass('icon-minus').addClass('icon-plus');
			}
		},
		expandNested : function(e) {						
			e.preventDefault();
			var parent = $(this).parent();
			if ( ! parent.hasClass('expanded'))
			{
				parent.addClass('expanded').removeClass('collapsed');
				$(this).find('.expand-icon').removeClass('icon-plus').addClass('icon-minus');
				$('.stars span', parent).each(ccm.util.stars);
			}
			else
			{	
				parent.removeClass('expanded').addClass('collapsed');
				$(this).find('.expand-icon').removeClass('icon-minus').addClass('icon-plus');
			}
		},
		collapseOnly : function(e) {
			e.preventDefault();
			var parent = $(this).parent();
			if ( ! parent.hasClass('expanded'))
			{
				parent.removeClass('collapsed').addClass('expanded').find('.expand-icon').removeClass('icon-plus').addClass('icon-minus');
			}else
			{
				parent.removeClass('expanded').addClass('collapsed').find('.expand-icon').removeClass('icon-minus').addClass('icon-plus');
			}			
		}, 
		toggle : function(e) {
			e.preventDefault();
			if (! $(this).hasClass('toggle-expanded')) {
				var list = $(this).parent().parent();
				$('a', list).removeClass('toggle-expanded');
				$('.hide', list).removeClass('show');				
			}
			$(this).toggleClass('toggle-expanded');
			var toggle_el = $(this).attr('data-toggle');
			$('#' + toggle_el).toggleClass('show');			
		},
		stars : function () {	
			var max_stars = 5;
			if($(this).find('em').size() > 0) {
				var max_width = $(this).width();
				var star_rating = parseFloat($(this).find('em').html());
				var star_width = Math.round((star_rating / max_stars) * max_width);
				$(this).find('em').css('width',star_width + 'px');
			}
		}
	}
	ccm.nav = {
		init : function() {
			ccm.nav.search_submitted = false;
			$('#search-wrap').on(touch_event, ccm.nav.showSearch);
			$('#search-wrap .btn-search').on(touch_event, ccm.nav.submitSearch);
			$('#search-wrap input').on('blur', ccm.nav.hideSearch);
		},
		submitSearch : function (e) {
			if(!$('#search-wrap').hasClass('closed')) {
				$('#search-wrap form').submit();
			}
		},
		showSearch : function (e) {
			if($('#search-wrap').hasClass('closed')) {
				$('#search-wrap').animate({width:'100%'},{duration: 200, complete : function () {
					$('#search-wrap').removeClass("closed");
					$('#search-wrap .btn-search').addClass('active');
					$('#search-wrap input[type="text"]').attr('placeholder','SEARCH MODEL OR KEYWORD').show();
					$('#search-field').focus();
				}});
			}
		},
		hideSearch : function(e) {
			e.stopPropagation();
			if(!$('#search-wrap').hasClass('closed')) {
				$('#search-wrap').animate({width:'19%'},{duration: 200, complete : function () {
					$('#search-wrap .btn-search').removeClass('active');
					$('#search-wrap').addClass("closed");
				}});
				$('#search-wrap input[type="text"]').attr({'placeholder':''}).hide();
			}
		}
	},
	ccm.slider = {
		init : function() {
			var slider = $('#slider');
			if (slider.length > 0) $('.slider-img', slider).each(ccm.optimize.lazyLoad);
			if ( Modernizr.csstransforms ) {
				if (slider.length > 0) {
					var sliderChildren = slider.find('li');
					if (sliderChildren.length > 1) {
						var sliderBullets = $('#slider-bullets');
						sliderChildren.each(function(index) {
							var em = $('<em></em>');
							if (index == 0) em.addClass('on');
							sliderBullets.append(em);
						});
						ccm.slider.cycle();
					}
				}
			}			
		},
		cycle : function() {
			var swipe = new Swipe(document.getElementById('slider'), {
				startSlide: 0,
				speed: 400,
				auto: 5000,
				callback: function(e, pos) {
					var i = bullets.length;
					while (i--) {
						bullets[i].className = '';
					}
					bullets[pos].className = 'on';
				}
			}),
			bullets = document.getElementById('slider-pos').getElementsByTagName('em');							
		}
	},
	ccm.locations = {		
		dom : {
			PRODUCT_TYPE_WRAP : $('.product-type-group'),
      PURCHASE_SOURCE_INPUT : 'input[name="purchase_source"]',
		},
		init : function() {
			$('.form-checkbox-location').on('click', ccm.locations.geocode);
			$('.radio-label').on('click', function () {
				var group = $(this).closest('.radio-group');
				$(group).find('.radio-label').removeClass('active');
				$(this).addClass('active');
			});
			$('input.zip').on('focus', function (){ $(this).hasClass('inactive') ? $('.form-checkbox-location').click() : ''; });
			// toggle product type
      $(this.dom.PURCHASE_SOURCE_INPUT).on('change', this.toggleProductType);
      this.toggleProductType.call($(this.dom.PURCHASE_SOURCE_INPUT + ':checked'), null);
		},
    toggleProductType : function (e) {
      if($(this).val() == "independent") {
        ccm.locations.dom.PRODUCT_TYPE_WRAP.show();
        ccm.locations.dom.PRODUCT_TYPE_WRAP.find('select').attr('required', 'required');
      } else {
        ccm.locations.dom.PRODUCT_TYPE_WRAP.hide();
        ccm.locations.dom.PRODUCT_TYPE_WRAP.find('select').attr('required', false);
      }
    },
		geocode : function(e) {
			if($('#location').size() > 0) {	
				var check = $('#location');
				if (e.target.type != 'checkbox') { 
					if (check.prop('checked')) {
						check.prop('checked', false);
					}else {
						check.prop('checked', true);
					}
			 	}
				var form = $(this).closest('form');
				if (check.prop('checked')) {
					$('.locate-by-zip-wrapper', form).hide();
					$('.loader', form).removeClass('hide');
					$('#zip', form).prop('required', false).addClass('inactive');
					$('#origaddress', form).prop('required', false).val('');
					$('#origcity', form).prop('required', false).val('');
					$('#origstateprovince', form).prop('required', false).val('');
					$('button[type="submit"]', form).hide();
					navigator.geolocation.getCurrentPosition(function(position) {
						$('#latitude', form).val(position.coords.latitude);
						$('#longitude', form).val(position.coords.longitude);
						$('.loader', form).addClass('hide');
						$('.form-checkbox-location', form).addClass('location-found');
						$('.form-checkbox-location', form).find('label').text('Current Location');
						$('.location-success', form).removeClass('hide');
						$('button[type="submit"]', form).show();
					}, function(error) {
						$('#latitude', form).val('');
						$('#longitude', form).val('');
						$('.locate-by-zip-wrapper', form).show();
						$('#location', form).prop('checked', false);
						alert('We\'re sorry, your location could not be determined. Please enter your zipcode to find stores in your area.');
						$('#latitude', form).val('');
						$('#longitude', form).val('');
						$('.locate-by-zip-wrapper', form).show();
						$('.loader', form).addClass('hide');
						$('.location-success', form).addClass('hide');
						$('.form-checkbox-location', form).removeClass('location-found');
						$('#zip', form).prop('required', false).removeClass('inactive');
						$('.form-checkbox-location', form).find('label').text('Use current location instead');
						$('button[type="submit"]', form).show();
						$('#zip', form).prop('required', true);
						$('#origaddress', form).prop('required', true);
						$('#origcity', form).prop('required', true);
						$('#origstateprovince', form).prop('required', true);
					});
				} else {
					$('#latitude', form).val('');
					$('#longitude', form).val('');
					$('.locate-by-zip-wrapper', form).show();
					$('.loader', form).addClass('hide');
					$('.location-success', form).addClass('hide');
					$('.form-checkbox-location', form).removeClass('location-found');
					$('.form-checkbox-location', form).find('label').text('Use current location instead');
					$('#zip', form).prop('required', false).removeClass('inactive');
					$('button[type="submit"]', form).show();
					$('#zip', form).prop('required', true);
					$('#origaddress', form).prop('required', true);
					$('#origcity', form).prop('required', true);
					$('#origstateprovince', form).prop('required', true);
				}
			} 
			else {
				$('#orig-location-fields').remove();
				navigator.geolocation.getCurrentPosition(function(position) {
					$('#latitude', form_store).attr('value', position.coords.latitude);
					$('#longitude', form_store).attr('value', position.coords.longitude);
				}, function(error) {
					alert('We\'re sorry, your location could not be determined. Please enter your zipcode to find stores in your area.');
					$('#latitude', form).val('');
					$('#longitude', form).val('');
					$('.locate-by-zip-wrapper', form).show();
					$('.loader', form).addClass('hide');
					$('.location-success', form).addClass('hide');
					$('.form-checkbox-location', form).removeClass('location-found');
					$('.form-checkbox-location', form).find('label').text('Use current location instead');
					$('#zip', form).prop('required', false).removeClass('inactive');
					$('button[type="submit"]', form).show();
					$('#zip', form).prop('required', true);
					$('#origaddress', form).prop('required', true);
					$('#origcity', form).prop('required', true);
					$('#origstateprovince', form).prop('required', true);
				});
			}			
		}
	},
	ccm.tabs = {
		init : function () {
			var num_tabs = 0;
			var has_hash = false;
			var hash_url = /^.+(#.+)$/;
			if(hash_url.test(document.URL)) {
				has_hash = document.URL.replace(hash_url, '$1');
			}
			$('.tabs a').each(function(i) {
				var target = $(this).attr('href');
				if($(this).hasClass('active') && !has_hash) {
					ccm.tabs.swapTabs(this);
				} else if(has_hash && has_hash == target) {
					ccm.tabs.swapTabs(this);
				}
				num_tabs++;
			});
			num_tabs > 0 ? ccm.tabs.setupHandlers() : '';
		},
		setupHandlers : function () {
			$('.tabs a').on(touch_event, function(e) {
				e.preventDefault();
				if(!$(this).hasClass('active')) {
					ccm.tabs.swapTabs(this);
				}
			});
		},
		swapTabs : function (el) {	
			var target = $(el).attr('href');
			$('.tabs a').removeClass('active');
			$(el).addClass('active');
			$('.tab-content').removeClass('current');
			$(target).addClass('current');
		}
	},
	ccm.products = {
		init: function() {
			if ($('#register-product-form').length > 0) {
				ccm.products.loadModels();
			}
		},
		loadModels: function() {
			$("#product-type").on("change", function(e) {
				var productType = $(this);
				var model = $('#model');
				var modelsHTML = '<option value="">Select (required)</option>';
				if (productType.val() != '') {
					$.get('/models/'+productType.val(), function(data) {
						var models = JSON.parse(data);
						$.each(models, function(i) {
							modelsHTML += '<option value="' + models[i].value + '">' + models[i].label + '</option>';
						});
						model.html(modelsHTML);
					});
				}else {
					model.html(modelsHTML);
				}
			});
		}
	},
	ccm.productFinder = {
		current_slot: 1,
		slot_data: [],
		start_url: '/products/finder/yard-size',
		setup: false,
		spinner_container: document.createElement('div'),
		init: function () {
			//make sure it exists before firing all this fancy code
			if($('#selector-widget').size() > 0) {
				//setup the vars
				ccm.productFinder.all_checked = 3;
				ccm.productFinder.num_checked = 0;
				
				$('#selector-widget').addClass('has-' + ccm.productFinder.all_checked + '-slots');
				//fetch the first set of data
				ccm.productFinder.getSelectorData();
			}
		},
		getSelectorData: function() {
			ccm.productFinder.loadSpinner($('#main'), "start");
			$.ajax({
				//default url is set at the top other urls come from json data
				url: (!ccm.productFinder.setup ? ccm.productFinder.start_url : ccm.productFinder.slot_data[ccm.productFinder.current_slot - 2].submit_url),
				//first time no data is sent. other requests send the answer in order to retrieve next dataset.
				data: { answer: (!ccm.productFinder.setup ? null : $('block').eq(ccm.productFinder.current_slot - 1).find('input.answer-value').val() ) },
				dataType: 'json',
				type: 'GET',
				success: function(data) {
					ccm.productFinder.slot_data.push(data);
					//build the form markup
					$(ccm.productFinder.slot_data).each(function (i) {
						this.id = i + 1;
						$(this.answers).each(function (j) {
							this.id = j + 1;
						});
					});
					var template_render = _.template($( "#selector-template" ).html());
					$('#selector-widget ul.expandable').append(template_render( ccm.productFinder.slot_data[ccm.productFinder.current_slot - 1] ));
					if(!ccm.productFinder.setup) {
						ccm.productFinder.productSelectorHandlers();
					}
					ccm.productFinder.setup = true;
					ccm.productFinder.loadSpinner($('#main'), "stop");
				}
			});
		},
		productSelectorHandlers: function () {
			//swap the image on mouseover
			$('#selector-widget').on('change', 'input.answer-value', function () {
				var selected_index = $($(this).closest('.block').find('input.answer-value')).index(this);
				var targ = this;
				$(this).closest('.block').find('span.img-caption').animate({ opacity: 0 }, {duration: 200, complete: function () {
					$(this).css('background-image', 'url(' + $(targ).data('image') + ')').animate({ opacity: 1 }, 200);
				}});
				$(this).closest('.block').find('.action').removeClass('invisible');
			});
			$('#product-finder-form').on('submit', function () {
				var selected_index = $('#selector-widget .expanded').index();
				ccm.productFinder.current_slot = (selected_index + 1);
				if(selected_index == ccm.productFinder.all_checked)
				{
					$(this).submit();
				}
				else
				{
					if(!$('#selector-widget .expanded').hasClass('answered')) {
						$('#selector-widget .expanded .answer').html($('#selector-widget .expanded input:checked').next('label').html());
						$('#selector-widget .expanded').removeClass('expanded').addClass('collapsed').addClass('answered');
						ccm.productFinder.getSelectorData();
					} else {
						$('#selector-widget .expanded').removeClass('expanded').addClass('collapsed');
						$('#selector-widget .cat-wrapper').each(function (i) {
							if(selected_index <= i) {
								$(this).remove();
								ccm.productFinder.slot_data.splice(i);
							}
						});
						ccm.productFinder.getSelectorData();
					}
				}
				return false;
			});
		},
		loadSpinner: function(parent_el, action) {
			var opts = {
			  lines: 12, // The number of lines to draw
			  length: 4, // The length of each line
			  width: 2, // The line thickness
			  radius: 5, // The radius of the inner circle
			  color: '#000', // #rgb or #rrggbb
			  speed: 1, // Rounds per second
			  trail: 60, // Afterglow percentage
			  shadow: false // Whether to render a shadow
			};
			if(!ccm.productFinder.setup) {
				$(parent_el).append(ccm.productFinder.spinner_container);
				ccm.productFinder.spinner = new Spinner(opts);
			}
			if(action == "start") {
				$(ccm.productFinder.spinner_container).css({'position' : 'absolute', 'left' : '49%', 'top' : '50%'});
				ccm.productFinder.spinner.spin(ccm.productFinder.spinner_container);
			} else if (action == "stop") {
				ccm.productFinder.spinner.stop();
			}
		}
	}
} (cubcadet.mobile));

$(function() {
	cubcadet.mobile.nav.init();
	cubcadet.mobile.slider.init();
	cubcadet.mobile.optimize.init();
	cubcadet.mobile.locations.init();
	cubcadet.mobile.cart.init();
	cubcadet.mobile.util.init();
	cubcadet.mobile.validate.init();
	cubcadet.mobile.controls.init();
	cubcadet.mobile.tabs.init();
	cubcadet.mobile.products.init();
	cubcadet.mobile.productFinder.init();
});

//     Underscore.js 1.4.2
//     http://underscorejs.org
//     (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
//     Underscore may be freely distributed under the MIT license.
(function(){var e=this,t=e._,n={},r=Array.prototype,i=Object.prototype,s=Function.prototype,o=r.push,u=r.slice,a=r.concat,f=r.unshift,l=i.toString,c=i.hasOwnProperty,h=r.forEach,p=r.map,d=r.reduce,v=r.reduceRight,m=r.filter,g=r.every,y=r.some,b=r.indexOf,w=r.lastIndexOf,E=Array.isArray,S=Object.keys,x=s.bind,T=function(e){if(e instanceof T)return e;if(!(this instanceof T))return new T(e);this._wrapped=e};typeof exports!="undefined"?(typeof module!="undefined"&&module.exports&&(exports=module.exports=T),exports._=T):e._=T,T.VERSION="1.4.2";var N=T.each=T.forEach=function(e,t,r){if(e==null)return;if(h&&e.forEach===h)e.forEach(t,r);else if(e.length===+e.length){for(var i=0,s=e.length;i<s;i++)if(t.call(r,e[i],i,e)===n)return}else for(var o in e)if(T.has(e,o)&&t.call(r,e[o],o,e)===n)return};T.map=T.collect=function(e,t,n){var r=[];return e==null?r:p&&e.map===p?e.map(t,n):(N(e,function(e,i,s){r[r.length]=t.call(n,e,i,s)}),r)},T.reduce=T.foldl=T.inject=function(e,t,n,r){var i=arguments.length>2;e==null&&(e=[]);if(d&&e.reduce===d)return r&&(t=T.bind(t,r)),i?e.reduce(t,n):e.reduce(t);N(e,function(e,s,o){i?n=t.call(r,n,e,s,o):(n=e,i=!0)});if(!i)throw new TypeError("Reduce of empty array with no initial value");return n},T.reduceRight=T.foldr=function(e,t,n,r){var i=arguments.length>2;e==null&&(e=[]);if(v&&e.reduceRight===v)return r&&(t=T.bind(t,r)),arguments.length>2?e.reduceRight(t,n):e.reduceRight(t);var s=e.length;if(s!==+s){var o=T.keys(e);s=o.length}N(e,function(u,a,f){a=o?o[--s]:--s,i?n=t.call(r,n,e[a],a,f):(n=e[a],i=!0)});if(!i)throw new TypeError("Reduce of empty array with no initial value");return n},T.find=T.detect=function(e,t,n){var r;return C(e,function(e,i,s){if(t.call(n,e,i,s))return r=e,!0}),r},T.filter=T.select=function(e,t,n){var r=[];return e==null?r:m&&e.filter===m?e.filter(t,n):(N(e,function(e,i,s){t.call(n,e,i,s)&&(r[r.length]=e)}),r)},T.reject=function(e,t,n){var r=[];return e==null?r:(N(e,function(e,i,s){t.call(n,e,i,s)||(r[r.length]=e)}),r)},T.every=T.all=function(e,t,r){t||(t=T.identity);var i=!0;return e==null?i:g&&e.every===g?e.every(t,r):(N(e,function(e,s,o){if(!(i=i&&t.call(r,e,s,o)))return n}),!!i)};var C=T.some=T.any=function(e,t,r){t||(t=T.identity);var i=!1;return e==null?i:y&&e.some===y?e.some(t,r):(N(e,function(e,s,o){if(i||(i=t.call(r,e,s,o)))return n}),!!i)};T.contains=T.include=function(e,t){var n=!1;return e==null?n:b&&e.indexOf===b?e.indexOf(t)!=-1:(n=C(e,function(e){return e===t}),n)},T.invoke=function(e,t){var n=u.call(arguments,2);return T.map(e,function(e){return(T.isFunction(t)?t:e[t]).apply(e,n)})},T.pluck=function(e,t){return T.map(e,function(e){return e[t]})},T.where=function(e,t){return T.isEmpty(t)?[]:T.filter(e,function(e){for(var n in t)if(t[n]!==e[n])return!1;return!0})},T.max=function(e,t,n){if(!t&&T.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.max.apply(Math,e);if(!t&&T.isEmpty(e))return-Infinity;var r={computed:-Infinity};return N(e,function(e,i,s){var o=t?t.call(n,e,i,s):e;o>=r.computed&&(r={value:e,computed:o})}),r.value},T.min=function(e,t,n){if(!t&&T.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.min.apply(Math,e);if(!t&&T.isEmpty(e))return Infinity;var r={computed:Infinity};return N(e,function(e,i,s){var o=t?t.call(n,e,i,s):e;o<r.computed&&(r={value:e,computed:o})}),r.value},T.shuffle=function(e){var t,n=0,r=[];return N(e,function(e){t=T.random(n++),r[n-1]=r[t],r[t]=e}),r};var k=function(e){return T.isFunction(e)?e:function(t){return t[e]}};T.sortBy=function(e,t,n){var r=k(t);return T.pluck(T.map(e,function(e,t,i){return{value:e,index:t,criteria:r.call(n,e,t,i)}}).sort(function(e,t){var n=e.criteria,r=t.criteria;if(n!==r){if(n>r||n===void 0)return 1;if(n<r||r===void 0)return-1}return e.index<t.index?-1:1}),"value")};var L=function(e,t,n,r){var i={},s=k(t);return N(e,function(t,o){var u=s.call(n,t,o,e);r(i,u,t)}),i};T.groupBy=function(e,t,n){return L(e,t,n,function(e,t,n){(T.has(e,t)?e[t]:e[t]=[]).push(n)})},T.countBy=function(e,t,n){return L(e,t,n,function(e,t,n){T.has(e,t)||(e[t]=0),e[t]++})},T.sortedIndex=function(e,t,n,r){n=n==null?T.identity:k(n);var i=n.call(r,t),s=0,o=e.length;while(s<o){var u=s+o>>>1;n.call(r,e[u])<i?s=u+1:o=u}return s},T.toArray=function(e){return e?e.length===+e.length?u.call(e):T.values(e):[]},T.size=function(e){return e.length===+e.length?e.length:T.keys(e).length},T.first=T.head=T.take=function(e,t,n){return t!=null&&!n?u.call(e,0,t):e[0]},T.initial=function(e,t,n){return u.call(e,0,e.length-(t==null||n?1:t))},T.last=function(e,t,n){return t!=null&&!n?u.call(e,Math.max(e.length-t,0)):e[e.length-1]},T.rest=T.tail=T.drop=function(e,t,n){return u.call(e,t==null||n?1:t)},T.compact=function(e){return T.filter(e,function(e){return!!e})};var A=function(e,t,n){return N(e,function(e){T.isArray(e)?t?o.apply(n,e):A(e,t,n):n.push(e)}),n};T.flatten=function(e,t){return A(e,t,[])},T.without=function(e){return T.difference(e,u.call(arguments,1))},T.uniq=T.unique=function(e,t,n,r){var i=n?T.map(e,n,r):e,s=[],o=[];return N(i,function(n,r){if(t?!r||o[o.length-1]!==n:!T.contains(o,n))o.push(n),s.push(e[r])}),s},T.union=function(){return T.uniq(a.apply(r,arguments))},T.intersection=function(e){var t=u.call(arguments,1);return T.filter(T.uniq(e),function(e){return T.every(t,function(t){return T.indexOf(t,e)>=0})})},T.difference=function(e){var t=a.apply(r,u.call(arguments,1));return T.filter(e,function(e){return!T.contains(t,e)})},T.zip=function(){var e=u.call(arguments),t=T.max(T.pluck(e,"length")),n=new Array(t);for(var r=0;r<t;r++)n[r]=T.pluck(e,""+r);return n},T.object=function(e,t){var n={};for(var r=0,i=e.length;r<i;r++)t?n[e[r]]=t[r]:n[e[r][0]]=e[r][1];return n},T.indexOf=function(e,t,n){if(e==null)return-1;var r=0,i=e.length;if(n){if(typeof n!="number")return r=T.sortedIndex(e,t),e[r]===t?r:-1;r=n<0?Math.max(0,i+n):n}if(b&&e.indexOf===b)return e.indexOf(t,n);for(;r<i;r++)if(e[r]===t)return r;return-1},T.lastIndexOf=function(e,t,n){if(e==null)return-1;var r=n!=null;if(w&&e.lastIndexOf===w)return r?e.lastIndexOf(t,n):e.lastIndexOf(t);var i=r?n:e.length;while(i--)if(e[i]===t)return i;return-1},T.range=function(e,t,n){arguments.length<=1&&(t=e||0,e=0),n=arguments[2]||1;var r=Math.max(Math.ceil((t-e)/n),0),i=0,s=new Array(r);while(i<r)s[i++]=e,e+=n;return s};var O=function(){};T.bind=function(t,n){var r,i;if(t.bind===x&&x)return x.apply(t,u.call(arguments,1));if(!T.isFunction(t))throw new TypeError;return i=u.call(arguments,2),r=function(){if(this instanceof r){O.prototype=t.prototype;var e=new O,s=t.apply(e,i.concat(u.call(arguments)));return Object(s)===s?s:e}return t.apply(n,i.concat(u.call(arguments)))}},T.bindAll=function(e){var t=u.call(arguments,1);return t.length==0&&(t=T.functions(e)),N(t,function(t){e[t]=T.bind(e[t],e)}),e},T.memoize=function(e,t){var n={};return t||(t=T.identity),function(){var r=t.apply(this,arguments);return T.has(n,r)?n[r]:n[r]=e.apply(this,arguments)}},T.delay=function(e,t){var n=u.call(arguments,2);return setTimeout(function(){return e.apply(null,n)},t)},T.defer=function(e){return T.delay.apply(T,[e,1].concat(u.call(arguments,1)))},T.throttle=function(e,t){var n,r,i,s,o,u,a=T.debounce(function(){o=s=!1},t);return function(){n=this,r=arguments;var f=function(){i=null,o&&(u=e.apply(n,r)),a()};return i||(i=setTimeout(f,t)),s?o=!0:(s=!0,u=e.apply(n,r)),a(),u}},T.debounce=function(e,t,n){var r,i;return function(){var s=this,o=arguments,u=function(){r=null,n||(i=e.apply(s,o))},a=n&&!r;return clearTimeout(r),r=setTimeout(u,t),a&&(i=e.apply(s,o)),i}},T.once=function(e){var t=!1,n;return function(){return t?n:(t=!0,n=e.apply(this,arguments),e=null,n)}},T.wrap=function(e,t){return function(){var n=[e];return o.apply(n,arguments),t.apply(this,n)}},T.compose=function(){var e=arguments;return function(){var t=arguments;for(var n=e.length-1;n>=0;n--)t=[e[n].apply(this,t)];return t[0]}},T.after=function(e,t){return e<=0?t():function(){if(--e<1)return t.apply(this,arguments)}},T.keys=S||function(e){if(e!==Object(e))throw new TypeError("Invalid object");var t=[];for(var n in e)T.has(e,n)&&(t[t.length]=n);return t},T.values=function(e){var t=[];for(var n in e)T.has(e,n)&&t.push(e[n]);return t},T.pairs=function(e){var t=[];for(var n in e)T.has(e,n)&&t.push([n,e[n]]);return t},T.invert=function(e){var t={};for(var n in e)T.has(e,n)&&(t[e[n]]=n);return t},T.functions=T.methods=function(e){var t=[];for(var n in e)T.isFunction(e[n])&&t.push(n);return t.sort()},T.extend=function(e){return N(u.call(arguments,1),function(t){for(var n in t)e[n]=t[n]}),e},T.pick=function(e){var t={},n=a.apply(r,u.call(arguments,1));return N(n,function(n){n in e&&(t[n]=e[n])}),t},T.omit=function(e){var t={},n=a.apply(r,u.call(arguments,1));for(var i in e)T.contains(n,i)||(t[i]=e[i]);return t},T.defaults=function(e){return N(u.call(arguments,1),function(t){for(var n in t)e[n]==null&&(e[n]=t[n])}),e},T.clone=function(e){return T.isObject(e)?T.isArray(e)?e.slice():T.extend({},e):e},T.tap=function(e,t){return t(e),e};var M=function(e,t,n,r){if(e===t)return e!==0||1/e==1/t;if(e==null||t==null)return e===t;e instanceof T&&(e=e._wrapped),t instanceof T&&(t=t._wrapped);var i=l.call(e);if(i!=l.call(t))return!1;switch(i){case"[object String]":return e==String(t);case"[object Number]":return e!=+e?t!=+t:e==0?1/e==1/t:e==+t;case"[object Date]":case"[object Boolean]":return+e==+t;case"[object RegExp]":return e.source==t.source&&e.global==t.global&&e.multiline==t.multiline&&e.ignoreCase==t.ignoreCase}if(typeof e!="object"||typeof t!="object")return!1;var s=n.length;while(s--)if(n[s]==e)return r[s]==t;n.push(e),r.push(t);var o=0,u=!0;if(i=="[object Array]"){o=e.length,u=o==t.length;if(u)while(o--)if(!(u=M(e[o],t[o],n,r)))break}else{var a=e.constructor,f=t.constructor;if(a!==f&&!(T.isFunction(a)&&a instanceof a&&T.isFunction(f)&&f instanceof f))return!1;for(var c in e)if(T.has(e,c)){o++;if(!(u=T.has(t,c)&&M(e[c],t[c],n,r)))break}if(u){for(c in t)if(T.has(t,c)&&!(o--))break;u=!o}}return n.pop(),r.pop(),u};T.isEqual=function(e,t){return M(e,t,[],[])},T.isEmpty=function(e){if(e==null)return!0;if(T.isArray(e)||T.isString(e))return e.length===0;for(var t in e)if(T.has(e,t))return!1;return!0},T.isElement=function(e){return!!e&&e.nodeType===1},T.isArray=E||function(e){return l.call(e)=="[object Array]"},T.isObject=function(e){return e===Object(e)},N(["Arguments","Function","String","Number","Date","RegExp"],function(e){T["is"+e]=function(t){return l.call(t)=="[object "+e+"]"}}),T.isArguments(arguments)||(T.isArguments=function(e){return!!e&&!!T.has(e,"callee")}),typeof /./!="function"&&(T.isFunction=function(e){return typeof e=="function"}),T.isFinite=function(e){return T.isNumber(e)&&isFinite(e)},T.isNaN=function(e){return T.isNumber(e)&&e!=+e},T.isBoolean=function(e){return e===!0||e===!1||l.call(e)=="[object Boolean]"},T.isNull=function(e){return e===null},T.isUndefined=function(e){return e===void 0},T.has=function(e,t){return c.call(e,t)},T.noConflict=function(){return e._=t,this},T.identity=function(e){return e},T.times=function(e,t,n){for(var r=0;r<e;r++)t.call(n,r)},T.random=function(e,t){return t==null&&(t=e,e=0),e+(0|Math.random()*(t-e+1))};var _={escape:{"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","/":"&#x2F;"}};_.unescape=T.invert(_.escape);var D={escape:new RegExp("["+T.keys(_.escape).join("")+"]","g"),unescape:new RegExp("("+T.keys(_.unescape).join("|")+")","g")};T.each(["escape","unescape"],function(e){T[e]=function(t){return t==null?"":(""+t).replace(D[e],function(t){return _[e][t]})}}),T.result=function(e,t){if(e==null)return null;var n=e[t];return T.isFunction(n)?n.call(e):n},T.mixin=function(e){N(T.functions(e),function(t){var n=T[t]=e[t];T.prototype[t]=function(){var e=[this._wrapped];return o.apply(e,arguments),F.call(this,n.apply(T,e))}})};var P=0;T.uniqueId=function(e){var t=P++;return e?e+t:t},T.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var H=/(.)^/,B={"'":"'","\\":"\\","\r":"r","\n":"n","	":"t","\u2028":"u2028","\u2029":"u2029"},j=/\\|'|\r|\n|\t|\u2028|\u2029/g;T.template=function(e,t,n){n=T.defaults({},n,T.templateSettings);var r=new RegExp([(n.escape||H).source,(n.interpolate||H).source,(n.evaluate||H).source].join("|")+"|$","g"),i=0,s="__p+='";e.replace(r,function(t,n,r,o,u){s+=e.slice(i,u).replace(j,function(e){return"\\"+B[e]}),s+=n?"'+\n((__t=("+n+"))==null?'':_.escape(__t))+\n'":r?"'+\n((__t=("+r+"))==null?'':__t)+\n'":o?"';\n"+o+"\n__p+='":"",i=u+t.length}),s+="';\n",n.variable||(s="with(obj||{}){\n"+s+"}\n"),s="var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n"+s+"return __p;\n";try{var o=new Function(n.variable||"obj","_",s)}catch(u){throw u.source=s,u}if(t)return o(t,T);var a=function(e){return o.call(this,e,T)};return a.source="function("+(n.variable||"obj")+"){\n"+s+"}",a},T.chain=function(e){return T(e).chain()};var F=function(e){return this._chain?T(e).chain():e};T.mixin(T),N(["pop","push","reverse","shift","sort","splice","unshift"],function(e){var t=r[e];T.prototype[e]=function(){var n=this._wrapped;return t.apply(n,arguments),(e=="shift"||e=="splice")&&n.length===0&&delete n[0],F.call(this,n)}}),N(["concat","join","slice"],function(e){var t=r[e];T.prototype[e]=function(){return F.call(this,t.apply(this._wrapped,arguments))}}),T.extend(T.prototype,{chain:function(){return this._chain=!0,this},value:function(){return this._wrapped}})}).call(this);

_.templateSettings = {
	interpolate: /\<\@\=(.+?)\@\>/gim,
	evaluate: /\<\@(.+?)\@\>/gim
};

//fgnass.github.com/spin.js#v1.2.7
!function(e,t,n){function o(e,n){var r=t.createElement(e||"div"),i;for(i in n)r[i]=n[i];return r}function u(e){for(var t=1,n=arguments.length;t<n;t++)e.appendChild(arguments[t]);return e}function f(e,t,n,r){var o=["opacity",t,~~(e*100),n,r].join("-"),u=.01+n/r*100,f=Math.max(1-(1-e)/t*(100-u),e),l=s.substring(0,s.indexOf("Animation")).toLowerCase(),c=l&&"-"+l+"-"||"";return i[o]||(a.insertRule("@"+c+"keyframes "+o+"{"+"0%{opacity:"+f+"}"+u+"%{opacity:"+e+"}"+(u+.01)+"%{opacity:1}"+(u+t)%100+"%{opacity:"+e+"}"+"100%{opacity:"+f+"}"+"}",a.cssRules.length),i[o]=1),o}function l(e,t){var i=e.style,s,o;if(i[t]!==n)return t;t=t.charAt(0).toUpperCase()+t.slice(1);for(o=0;o<r.length;o++){s=r[o]+t;if(i[s]!==n)return s}}function c(e,t){for(var n in t)e.style[l(e,n)||n]=t[n];return e}function h(e){for(var t=1;t<arguments.length;t++){var r=arguments[t];for(var i in r)e[i]===n&&(e[i]=r[i])}return e}function p(e){var t={x:e.offsetLeft,y:e.offsetTop};while(e=e.offsetParent)t.x+=e.offsetLeft,t.y+=e.offsetTop;return t}var r=["webkit","Moz","ms","O"],i={},s,a=function(){var e=o("style",{type:"text/css"});return u(t.getElementsByTagName("head")[0],e),e.sheet||e.styleSheet}(),d={lines:12,length:7,width:5,radius:10,rotate:0,corners:1,color:"#000",speed:1,trail:100,opacity:.25,fps:20,zIndex:2e9,className:"spinner",top:"auto",left:"auto",position:"relative"},v=function m(e){if(!this.spin)return new m(e);this.opts=h(e||{},m.defaults,d)};v.defaults={},h(v.prototype,{spin:function(e){this.stop();var t=this,n=t.opts,r=t.el=c(o(0,{className:n.className}),{position:n.position,width:0,zIndex:n.zIndex}),i=n.radius+n.length+n.width,u,a;e&&(e.insertBefore(r,e.firstChild||null),a=p(e),u=p(r),c(r,{left:(n.left=="auto"?a.x-u.x+(e.offsetWidth>>1):parseInt(n.left,10)+i)+"px",top:(n.top=="auto"?a.y-u.y+(e.offsetHeight>>1):parseInt(n.top,10)+i)+"px"})),r.setAttribute("aria-role","progressbar"),t.lines(r,t.opts);if(!s){var f=0,l=n.fps,h=l/n.speed,d=(1-n.opacity)/(h*n.trail/100),v=h/n.lines;(function m(){f++;for(var e=n.lines;e;e--){var i=Math.max(1-(f+e*v)%h*d,n.opacity);t.opacity(r,n.lines-e,i,n)}t.timeout=t.el&&setTimeout(m,~~(1e3/l))})()}return t},stop:function(){var e=this.el;return e&&(clearTimeout(this.timeout),e.parentNode&&e.parentNode.removeChild(e),this.el=n),this},lines:function(e,t){function i(e,r){return c(o(),{position:"absolute",width:t.length+t.width+"px",height:t.width+"px",background:e,boxShadow:r,transformOrigin:"left",transform:"rotate("+~~(360/t.lines*n+t.rotate)+"deg) translate("+t.radius+"px"+",0)",borderRadius:(t.corners*t.width>>1)+"px"})}var n=0,r;for(;n<t.lines;n++)r=c(o(),{position:"absolute",top:1+~(t.width/2)+"px",transform:t.hwaccel?"translate3d(0,0,0)":"",opacity:t.opacity,animation:s&&f(t.opacity,t.trail,n,t.lines)+" "+1/t.speed+"s linear infinite"}),t.shadow&&u(r,c(i("#000","0 0 4px #000"),{top:"2px"})),u(e,u(r,i(t.color,"0 0 1px rgba(0,0,0,.1)")));return e},opacity:function(e,t,n){t<e.childNodes.length&&(e.childNodes[t].style.opacity=n)}}),function(){function e(e,t){return o("<"+e+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',t)}var t=c(o("group"),{behavior:"url(#default#VML)"});!l(t,"transform")&&t.adj?(a.addRule(".spin-vml","behavior:url(#default#VML)"),v.prototype.lines=function(t,n){function s(){return c(e("group",{coordsize:i+" "+i,coordorigin:-r+" "+ -r}),{width:i,height:i})}function l(t,i,o){u(a,u(c(s(),{rotation:360/n.lines*t+"deg",left:~~i}),u(c(e("roundrect",{arcsize:n.corners}),{width:r,height:n.width,left:n.radius,top:-n.width>>1,filter:o}),e("fill",{color:n.color,opacity:n.opacity}),e("stroke",{opacity:0}))))}var r=n.length+n.width,i=2*r,o=-(n.width+n.length)*2+"px",a=c(s(),{position:"absolute",top:o,left:o}),f;if(n.shadow)for(f=1;f<=n.lines;f++)l(f,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(f=1;f<=n.lines;f++)l(f);return u(t,a)},v.prototype.opacity=function(e,t,n,r){var i=e.firstChild;r=r.shadow&&r.lines||0,i&&t+r<i.childNodes.length&&(i=i.childNodes[t+r],i=i&&i.firstChild,i=i&&i.firstChild,i&&(i.opacity=n))}):s=l(t,"animation")}(),typeof define=="function"&&define.amd?define(function(){return v}):e.Spinner=v}(window,document);

