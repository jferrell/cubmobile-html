var storelocator = (typeof storelocator !== 'undefined') ? storelocator : {};

(function (stlc) {

  stlc.maps = {
    dom : {
      ADD_FORMS_CLASS : 'add-forms',
      FILTER_ALL_STORES_ID : "all-stores",
      FILTER_CATEGORIES : $('[data-filter-categories]'),
      FILTER_CATEGORIES_DATA_ATTR : "filter-categories",
      FILTER_CONTAINER : $('.filter-container'),
      FIND_STORE_FORM_CLASS : ".find-store-form",
      FORM_TOGGLE_CLASS : "form-open",
      HIDDEN_CLASS : 'hide',
      INFOWINDOW_ROLLOVER_TEMPLATE : $('#infowindow-rollover-template'),
      LOADING_CLASS : 'loading',
      LOCATION_DATA : $('#data-locations'),
      MAP : $('#stores-map'),
      MAP_ADDRESS_LINK : $('[data-address-link]'),
      MAP_ADDRESS_LINK_DATA_ATTR : 'address-link',
      MAP_CONTAINER : $('#map-container'),
      MAP_TOGGLE_LINK : '.map-view-toggle',
      OVERLAY : $('.overlay'),
      STORE : $('#stores-list .store'),
      TOGGLE_DELEGATE_TARGET : $('.store-locator .stores .store'),
      TOGGLE_MAP_FILTERS_TRIGGER : $('#toggle-map-filters'),
      TOGGLE_TRIGGER_CLASS : ".toggle-form",
      ZIP_FIELD_NAME : "zip"
    },
    settings : {
      MAPQUEST_KEY : "Gmjtd%7Cluua2hubnl%2Cbn%3Do5-lz8l5"
    },
    map_opts : function () {
      return {
        elt:document.getElementById(this.dom.MAP.attr("id")),
        zoom:10,
        mtype:'map',
        bestFitMargin:0,
        zoomOnDoubleClick:true
      }
    },
    init : function () {
      // set var for map setup
      this.map_setup = false;
      // check for json data
      this.checkData();
      // build store object and create event handlers
      this.setupStores();
      // utils for showing / hiding and toggling classes
      stlc.utils.toggleClass(this.dom.MAP_CONTAINER);
      stlc.utils.toggleVisible(this.dom.MAP_CONTAINER);
      stlc.utils.triggerClick(this.dom.MAP_CONTAINER);
      // check user agent      
      this.dom.MAP_ADDRESS_LINK.each(stlc.maps.makeiOSMapLink);
      // map toggle updates
      this.dom.OVERLAY.on('class_changed', '#' + this.dom.MAP.attr('id'), this.toggleMap);
    },
    checkiOS : function () {
      return ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
    },
    makeiOSMapLink : function () {
      if(stlc.maps.checkiOS()) {
        $(this).attr('href', stlc.maps.getiOSMapLink(this));
      }
    },
    getiOSMapLink : function (el) {
      return 'http://maps.apple.com/?q=' + $(el).data(stlc.maps.dom.MAP_ADDRESS_LINK_DATA_ATTR);
    },
    checkData : function () {
      this.location_data = this.dom.LOCATION_DATA.size() > 0 ? JSON.parse(stlc.maps.dom.LOCATION_DATA.html()).locations : false;
      this.brand = 'cubcadet';
      // current stores
      this.current_stores = this.location_data;
    },
    toggleMap : function (e) {
      if($(e.target).hasClass('expanded') && !stlc.maps.map_setup) {
        stlc.maps.dom.MAP_CONTAINER.addClass(stlc.maps.dom.LOADING_CLASS);
        setTimeout(function () {
          stlc.maps.resultsMap();
        }, 1500);
      }
    },
    setupStores : function () {
      var stores = [];
      this.dom.STORE.each(function () {
        var store = new stlc.store(this);
        stores.push(store);
      });
      stlc.maps.locationFilters().on('change', function () {
        stlc.maps.current_stores = [];
        if($(this).attr("id") == stlc.maps.dom.FILTER_ALL_STORES_ID && $(this).is(":checked")) {
          stlc.maps.locationFilters().not("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).attr("checked", false);
        } else if($(this).attr("id") != stlc.maps.dom.FILTER_ALL_STORES_ID && $(this).is(":checked")) {
          $("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).attr("checked", false);
        } else if(stlc.maps.locationFilters().filter(":checked").size() == 0) {
          $("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).attr("checked", "checked");
        }
        var categories = [];
        // handle if "all stores not present"
        stlc.maps.locationFilters().filter(":checked").each(function () {
          categories = categories.concat(this.categories);
        });
        if($("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).size() == 0 && stlc.maps.locationFilters().filter(":checked").size() == 0) {
          categories = [];
        } else if ($("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).size() > 0 && $("#" + stlc.maps.dom.FILTER_ALL_STORES_ID).is(':checked')) {
          categories = [];
        }
        $(stores).each(function () {
          var _store = this;
          var cats_missing = false;

          $(categories).each(function (i) {
            if(!stlc.utils.arrayContains.call(_store.categories, categories[i])) {
              cats_missing = true;
            }
          });

          if(cats_missing && categories.length > 0) {
            _store.events.hideStore();
          } else {
            for(key in stlc.maps.location_data) {
              if(typeof stlc.maps.location_data[key] === "object") {
                if(stlc.maps.location_data[key].id.toString() === _store.el.attr('id').toString()) {
                  stlc.maps.current_stores[key]= stlc.maps.location_data[key];
                }
              }
            }
            _store.events.showStore();
          }
        });
        if(stlc.maps.map_setup) {
          stlc.maps.setPoints(stlc.maps.current_stores);
        }
      });
    },
    resultsMap : function () {
      if(this.location_data && !this.dom.MAP.hasClass(this.dom.HIDDEN_CLASS)) {
        
        window.map = new MQA.TileMap(this.map_opts());
        MQA.withModule('smallzoom', function() {
          map.addControl(
            new MQA.SmallZoom(),
            new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
          );
        });
        MQA.withModule('htmlpoi', function() {
          stlc.maps.setPoints(stlc.maps.current_stores);
          stlc.maps.dom.MAP_CONTAINER.removeClass(stlc.maps.dom.LOADING_CLASS);
        });
      }
    },
    setPoints : function (coordinates) {
      map.removeAllShapes();
      stlc.maps.markers = [];
      // marker overlap fix
      coordinates.reverse();
      for(var key in coordinates) {
        var marker=new MQA.HtmlPoi( {lat: parseFloat(coordinates[key].latitude), lng: parseFloat(coordinates[key].longitude)} );
        if(coordinates[key].independent) {
          marker.setHtml(coordinates[key].index, 0, -10, 'map-marker-independent');
        } else {
          marker.setHtml(coordinates[key].index, 0, -10, 'map-marker');
        }


        marker.setInfoContentHTML('<h3>' + coordinates[key].name + '</h3>' + 
        '<a class="map-link" href="' + (stlc.maps.checkiOS() ? 'http://maps.apple.com/?q=' : 'http://maps.google.com/maps?q=') + coordinates[key].street + '+' + coordinates[key].city + '+' + coordinates[key].state + '+' + coordinates[key].zip + '">' +
        coordinates[key].street + '<br />' + 
        coordinates[key].city + ', ' + 
        '<span class="state">' + coordinates[key].state + '</span> ' + 
        coordinates[key].zip + 
        '</a>' +
        '<br />' +
        '<a class="phone-link" href="tel:' + coordinates[key].phone + '" class="phone">' +
        '<strong>' + coordinates[key].phone + '</strong></a>');

        if(stlc.maps.dom.INFOWINDOW_ROLLOVER_TEMPLATE.size() > 0) {
          var _template = $('<div />');
          _template.append(stlc.maps.dom.INFOWINDOW_ROLLOVER_TEMPLATE.html());
          _template.find('.name').html(coordinates[key].name);
          marker.setRolloverContent(_template.html());
        } else {
          marker.setRolloverContent('<h3>' + coordinates[key].name + '</h3>' + 'click marker for more info');
        }
        
        map.addShape(marker);
        MQA.EventManager.addListener(marker, 'click', function (ev) {
          // todo: figure out if possible to manually turn off info window on click and also turn off hover window.  
        });
        stlc.maps.markers[key] = marker;
      }
      if(!stlc.maps.map_setup) {
        map.bestFit();
        stlc.maps.map_setup = true;
      }
    },
    locationFilters : function () {
      var _self = this;
      var filters = _self.dom.FILTER_CATEGORIES;
      filters.each(function () {
        if(typeof $(this).data(_self.dom.FILTER_CATEGORIES_DATA_ATTR).split === "function") {
          this.categories = $(this).data(_self.dom.FILTER_CATEGORIES_DATA_ATTR).split(',');
        } else {
          this.categories = [$(this).data(_self.dom.FILTER_CATEGORIES_DATA_ATTR).toString()];
        }
      });
      return filters;
    }
  },
  stlc.store = function (el) {
    var _self = this;
    _self.dom = {
      CATEGORIES_DATA_ATTR : "categories",
      HIDE_CLASS : "collapse"
    };
    // construct
    _self.el = $(el);
    // product lines
    if(typeof _self.el.data(_self.dom.CATEGORIES_DATA_ATTR).split === "function") {
      _self.categories = _self.el.data(_self.dom.CATEGORIES_DATA_ATTR).split(',');
    } else {
      _self.categories = [_self.el.data(_self.dom.CATEGORIES_DATA_ATTR).toString()];
    }
    // filters
    _self.filters = stlc.maps.locationFilters();
    _self.events = {
      hideStore : function () {
        _self.el.addClass(_self.dom.HIDE_CLASS);
      },
      showStore : function () {
        _self.el.removeClass(_self.dom.HIDE_CLASS);
      }
    }
  },
  stlc.utils = {
    dom : {
      TOGGLE_CLASS_TRIGGER : '[data-toggle-class]',
      TOGGLE_CLASS_DATA_ATTR : 'toggle-class',
      TOGGLE_VISIBLE_TRIGGER : '[data-toggle-visible]',
      TOGGLE_VISIBLE_DATA_ATTR : 'toggle-visible',
      CLICK_TRIGGER : '[data-trigger-click]',
      CLICK_TRIGGER_DATA_ATTR : 'trigger-click',
      PRINT_DIRECTIONS : '.print-directions'
    },
    toggleClass : function (_parent) {
      _parent.on('click', this.dom.TOGGLE_CLASS_TRIGGER, function (e) {
        $(this).toggleClass($(this).data(stlc.utils.dom.TOGGLE_CLASS_DATA_ATTR));
        e.preventDefault();
      });
    },
    triggerClick : function (_parent) {
      _parent.on('click', this.dom.CLICK_TRIGGER, function (e, ignore) {
        if(typeof ignore === "undefined") {
          $($(this).data(stlc.utils.dom.CLICK_TRIGGER_DATA_ATTR)).trigger('click', [true]);
          e.preventDefault();
        }
      });
    },
    toggleVisible : function (_parent) {
      _parent.on('click', this.dom.TOGGLE_VISIBLE_TRIGGER, function (e) {
        if($($(this).data(stlc.utils.dom.TOGGLE_VISIBLE_DATA_ATTR)).hasClass('hide')) {
          $($(this).data(stlc.utils.dom.TOGGLE_VISIBLE_DATA_ATTR)).removeClass('hide');
        } else {
          $($(this).data(stlc.utils.dom.TOGGLE_VISIBLE_DATA_ATTR)).addClass('hide');
        }
        $(this).trigger('class_changed');
        e.preventDefault();
      });
    },
    printDirections : function (_parent) {
      _parent.on('click', this.dom.PRINT_DIRECTIONS, function (e) {
        window.print();
        e.preventDefault();
      });
    },
    arrayContains : function (needle) {
      for (i in this) {
        if (this[i] == needle) {
          return true;
        }
      }
      return false;
    }
  }

} (storelocator));

$(function() {
  storelocator.maps.init();
});

(function() {
  var mod = 'modernizr2',
      modElem = document.createElement(mod),
      mStyle = modElem.style,
      omPrefixes = 'Webkit Moz O ms',
      cssomPrefixes = omPrefixes.split(' '),
      domPrefixes = omPrefixes.toLowerCase().split(' ');

  function is( obj, type ) {
    return typeof obj === type;
  }

  function contains( str, substr ) {
    return !!~('' + str).indexOf(substr);
  }

  function testProps( props, prefixed ) {
    for ( var i in props ) {
      var prop = props[i];
      if ( !contains(prop, "-") && mStyle[prop] !== undefined ) {
        return prefixed == 'pfx' ? prop : true;
      }
    }
    return false;
  }

  function testDOMProps( props, obj, elem ) {
    for ( var i in props ) {
      var item = obj[props[i]];
      if ( item !== undefined) {
        if (elem === false) return props[i];
        if (is(item, 'function')){
          return item.bind(elem || obj);
        }
        return item;
      }
    }
    return false;
  }

  function testPropsAll( prop, prefixed, elem ) {
    var ucProp  = prop.charAt(0).toUpperCase() + prop.slice(1),
        props   = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');
    if(is(prefixed, "string") || is(prefixed, "undefined")) {
      return testProps(props, prefixed);
    } else {
      props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
      return testDOMProps(props, prefixed, elem);
    }
  }

  var prefixed = function(prop, obj, elem){
    if(!obj) {
      return testPropsAll(prop, 'pfx');
    } else {
      return testPropsAll(prop, obj, elem);
    }
  };

  var triggerBttn = document.getElementById( 'trigger-overlay' ),
      overlay = document.querySelector( 'div.overlay' ),
      closeBttn = overlay.querySelector( 'button.overlay-close' );
      transEndEventNames = {
        'WebkitTransition': 'webkitTransitionEnd',
        'MozTransition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'msTransition': 'MSTransitionEnd',
        'transition': 'transitionend'
      },
      transEndEventName = transEndEventNames[ prefixed( 'transition' ) ],
      support = { transitions : supportsTransitions() };

  function supportsTransitions() {
    var b = document.body || document.documentElement;
    var s = b.style;
    var p = 'transition';
    if(typeof s[p] == 'string') {return true; }

    // Tests for vendor specific prop
    v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'],
    p = p.charAt(0).toUpperCase() + p.substr(1);
    for(var i=0; i<v.length; i++) {
      if(typeof s[v[i] + p] == 'string') { return true; }
    }
    return false;
  }

  function toggleOverlay() {
    if( $(overlay).hasClass('open') ) {
      $(overlay).removeClass('open');
      $(overlay).addClass('close');
      var onEndTransitionFn = function( ev ) {
        if( support.transitions ) {
          if( ev.propertyName !== 'visibility' ) return;
          this.removeEventListener( transEndEventName, onEndTransitionFn );
        }
        $(overlay).removeClass('close');
        $('#stores-map').removeClass('expanded');
      };
      if( support.transitions ) {
        overlay.addEventListener( transEndEventName, onEndTransitionFn );
      }
      else {
        onEndTransitionFn();
      }
    }
    else if( !$(overlay).hasClass('close') ) {
      $(overlay).addClass('open');
      $('#stores-map').addClass('expanded').trigger('class_changed');
    }
  }

  triggerBttn.addEventListener( 'click', toggleOverlay );
  closeBttn.addEventListener( 'click', toggleOverlay );
})();