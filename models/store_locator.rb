class StoreLocator

  attr_accessor :index, :id, :name, :street, :city, :state, :zip, :phone, :latitude, :longitude, :store_logo, :categories, :independent, :limited, :website, :distance

  @@stores = {
    'cubcadet' => [
      {
        "index" => 1,
        "id" => 1,
        "independent" => true,
        "limited" => false,
        "distance" => 4.7,
        "website" => "#",
        "name" => "Chilton Turf Center",
        "street" => "607 Craighead St",
        "city" => "Nashville",
        "state" => "TN",
        "zip" => "37204",
        "phone" => "(615) 254-1637",
        "latitude" => 36.126364,
        "longitude" => -86.775242,
        "store_logo" => "",
        "categories" => "1,2,3,4,5,6,7,8"
      },
      {
        "index" => 2,
        "id" => 2,
        "independent" => false,
        "limited" => true,
        "distance" => 5.2,
        "website" => "#",
        "name" => "The Home Depot",
        "street" => "2535 Powell Ave",
        "city" => "Nashville",
        "state" => "TN",
        "zip" => "37204",
        "phone" => "(615) 269-7800",
        "latitude" => 36.107176,
        "longitude" => -86.766219,
        "store_logo" => "",
        "categories" => "2,3,4,5,6"
      },
      {
        "index" => 3,
        "id" => 3,
        "independent" => false,
        "limited" => true,
        "distance" => 6.8,
        "website" => "#",
        "name" => "The Home Depot",
        "street" => "7665 Highway 70 South",
        "city" => "Nashville",
        "state" => "TN",
        "zip" => "37221",
        "phone" => "(615) 662-7600",
        "latitude" => 36.078685,
        "longitude" => -86.952150,
        "store_logo" => "",
        "categories" => "2,3,4,5,6"
      },
      {
        "index" => 4,
        "id" => 4,
        "independent" => true,
        "limited" => false,
        "distance" => 7.5,
        "website" => "#",
        "name" => "McHenry Tractor Co Inc",
        "street" => "1402 Dickerson Rd",
        "city" => "Nashville",
        "state" => "TN",
        "zip" => "37207",
        "phone" => "(615) 227-8291",
        "latitude" => 36.245123,
        "longitude" => -86.756482,
        "store_logo" => "",
        "categories" => "1,2,3,4,5,6,7,8"
      },
      {
        "index" => 5,
        "id" => 5,
        "independent" => false,
        "limited" => true,
        "distance" => 8.2,
        "website" => "#",
        "name" => "Tractor Supply Co.",
        "street" => "1101 Hillview Ln",
        "city" => "Franklin",
        "state" => "TN",
        "zip" => "37064",
        "phone" => "(615) 227-8291",
        "latitude" => 35.890805,
        "longitude" => -86.877945,
        "store_logo" => "",
        "categories" => "2,3,4,5"
      },
      {
        "index" => 6,
        "id" => 6,
        "independent" => false,
        "limited" => true,
        "distance" => 9.7,
        "website" => "#",
        "name" => "Tractor Supply Co.",
        "street" => "356 Frey St",
        "city" => "Ashland City",
        "state" => "TN",
        "zip" => "37015",
        "phone" => "(615) 792-1903",
        "latitude" => 36.297658,
        "longitude" => -87.048339,
        "store_logo" => "",
        "categories" => "2,3,4,5"
      }
    ]
  }

  @@categories = [
    {
      "id" => "1",
      "slug" => "category-professional",
      "name" => "Professional"
    },
    {
      "id" => "2",
      "slug" => "category-lawn-garden",
      "name" => "Lawn &amp; Garden Tractors"
    },
    {
      "id" => "3",
      "slug" => "category-zero-turn",
      "name" => "Zero-Turn Riding Mowers"
    },
    {
      "id" => "4",
      "slug" => "category-walk-behind",
      "name" => "Walk Behind Mowers"
    },
    {
      "id" => "5",
      "slug" => "category-handheld",
      "name" => "Handheld &amp; Cleanup"
    },
    {
      "id" => "6",
      "slug" => "category-snow-thrower",
      "name" => "Snow Throwers"
    },
    {
      "id" => "7",
      "slug" => "category-utility",
      "name" => "Utility Vehicles"
    },
    {
      "id" => "8",
      "slug" => "category-service-parts",
      "name" => "Service &amp; Parts"
    }
  ]

  def initialize(obj)
    obj.keys.each do |k|
      instance_variable_set "@#{k}", obj[k]
    end
  end

  def self.stores(brand)
    @@stores[brand].map { |s| StoreLocator.new(s) }
  end

  def self.categories
    @@categories
  end

  def self.find(id, brand)
    store_collection = @@stores[brand].select{|store| store["id"] == id.to_i }
    StoreLocator.new(store_collection[0])
  end

  def self.to_json(arry)
    locations = []
    arry.each_with_index do |s, i|
      store = {}
      s.instance_variables.each do |n|  
        store[:"#{n.to_s.sub('@','')}"] = s.instance_variable_get(n)
      end
      locations << store
    end
    {:locations => locations}.to_json
  end

end