class Cart
    
  def self.add_item(cart, id, price, quantity)
    @product = Product.get(id)
    item = { 
      id => { 
        'product_name' => @product['name'], 
        'product_item' => @product['item'], 
        'product_price' => price, 
        'product_sale_price' => @product['sale_price'],
        'product_quantity' => quantity, 
        'product_photo' => @product['photo'], 
        'product_photo_hd' => @product['photo_hd'], 
        'product_backordered' => @product['backordered'], 
        'product_is_part' => @product['is_part'] 
      }
    }
    if cart
      self.update_item_qty(cart, id, quantity, item)
    else
      item
    end
  end
  
  def self.update_item(cart, id, quantity)
    if quantity.to_i <= 0
      cart.delete(id)
    else
      cart[id]['product_quantity'] = quantity.to_s
    end
    cart
  end
  
  def self.delete_item(cart, id)
    cart.delete(id)
    if cart.size > 0
      cart
    else
      cart = nil
    end
  end
  
  def self.update_item_qty(cart, id, new_qty, item)
    if cart
      if cart.has_key?(id)
        qty = cart[id]['product_quantity'].to_i + new_qty.to_i
        cart[id]['product_quantity'] = qty.to_s
        cart
      else
        cart.merge(item)
      end
    end
  end
  
  def self.total_items(cart)
    total = 0
    if cart
      cart.each do |k,v|
        total = total + v['product_quantity'].to_i
      end
    end
    total
  end
  
  def self.total_amount(cart)
    amount = 0
    if cart
      cart.each do |k,v|
        amount = amount + (v['product_quantity'].to_i * v['product_price'].to_f)
      end
    end
    amount.round(2)
  end
  
end