class ProductFinder

  @categories = {
    'yard-size' => {
      "question" => "How much land do you need to mow?",
      "input_name" => "surface",
      "answers" => {
        "under-half-acre" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "Less than 1/2 acres"
        },
        "half-to-one-acre" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon-alt.jpg",
          "option" => "1/2 to 1 acres"
        },
        "one-to-two-acre" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "1 to 2 acres"
        },
        "over-two-acre" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon-alt.jpg",
          "option" => "Over 2 acres"
        }
      },
      "label" => "Yard Size",
      "submit_url" => "/products/finder/terrain",
      "action_text" => "Continue"
    },
    'terrain' => {
      "question" => "What type of terrain do you have?",
      "input_name" => "terrain",
      "answers" => {
        "flat" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "Flat"
        },
        "uneven" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "Uneven"
        },
        "hilly" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon-alt.jpg",
          "option" => "Hilly"
        }
      },
      "label" => "Terrain",
      "submit_url" => "/products/finder/obstacles",
      "action_text" => "Continue"
    },
    'obstacles' => {
      "question" => "How many obstacles will you need to mow around?",
      "input_name" => "obstacles",
      "answers" => {
        "none" => { 
          "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon-alt.jpg",
          "option" => "None"
        },
        "few" => { "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "A few"
        },
        "many" => { "image" => "/wcpics/CubCadetMobile/en_US/images/content/fpo-question-icon.jpg",
          "option" => "Many",
        }
      },
      "label" => "Obstacles",
      "submit_url" => "/products/finder/results",
      "action_text" => "Find matching Models"
    }
  }

  def self.get_all()
    @categories
  end
  
  def self.get(id)
    @categories[id]
  end

end