require 'json'

class Model

	@models = {
    'CR' => [
    	{'value' => 'GT1554','label' => 'GT 1554'},
    	{'value' => 'GTX2000','label' => 'GTX 2000'},
    	{'value' => 'GT2542','label' => 'GT 2542'}
    ],
    'CZ' => [
    	{'value' => 'FMZ50','label' => 'FMZ50'},
    	{'value' => 'TANKSZ60','label' => 'TANK SZ 60'},
    	{'value' => 'i1046','label' => 'i1046'},
    	{'value' => 'i1050','label' => 'i1050'}
    ],
    'CW' => [
    	{'value' => 'E18J','label' => 'E 18J'},
    	{'value' => 'CC999ES','label' => 'CC 999ES'},
    	{'value' => 'G1332','label' => 'G1332'}
    ],
    'CS' => [
    	{'value' => '221HP','label' => '221 HP'},
    	{'value' => '221LHP','label' => '221 LHP'},
    	{'value' => '524SWE','label' => '524 SWE'}
    ],
    'CP' => [
    	{'value' => 'CSV060K','label' => 'CSV 060K'},
    	{'value' => 'CSV050','label' => 'CSV 050'}
    ],
    'CT' => [
    	{'value' => 'ST227C','label' => 'ST 227C'},
    	{'value' => 'CCBT','label' => 'CC BT'},
    	{'value' => 'ST426C','label' => 'ST 426C'},
    	{'value' => 'ST426S','label' => 'ST 426S'}
    ],
    'CU' => [
    	{'value' => '4x4Yellow','label' => '4x4 Yellow'},
    	{'value' => '4x4TrailDiesel','label' => '4x4 Trail Diesel'},
    	{'value' => '4x4 EFI Yellow','label' => '4x4 EFI Yellow'},
    	{'value' => '4x4EFIRed','label' => '4x4 EFI Red'}
    ],
    'CA' => [
    	{'value' => 'DRIVEP68','label' => 'DRIVE P68'}
    ]
  }

  def self.get(id)
    @models[id].to_json
  end

end