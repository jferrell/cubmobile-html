class Store
  
  def self.stores
    @stores = {
      "home-depot" => { 
        :name => "Home Depot",
        :short_name => "thd",
        :image => "/wcpics/CubCadetMobile/en_US/images/content/home-depot-large.png",
        :image_hd => "/wcpics/CubCadetMobile/en_US/images/content/home-depot-large-2x.png",
        :products => [
          { 
            :name => 'Product 1', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden-2x.png' 
          },
          { 
            :name => 'Product 2', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/riding.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/riding-2x.png'
          },
          { 
            :name => 'Product 3', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind-2x.png' 
          }
        ],
        :other_products => [
          { 
            :name => 'Category 1', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/handheld.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/handheld-2x.png' 
          },
          { 
            :name => 'Category 2', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers-2x.png'
          },
          { 
            :name => 'Category 3', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles-2x.png'
          }
        ]
      },
      "tractor-supply" => { 
        :name => "Tractor Supply",
        :short_name => "tsc",
        :image => "/wcpics/CubCadetMobile/en_US/images/content/tractor-supply-large.png",
        :image_hd => "/wcpics/CubCadetMobile/en_US/images/content/tractor-supply-large-2x.png",
        :products => [
          { 
            :name => 'Product 1', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden-2x.png' 
          },
          { 
            :name => 'Product 2', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/riding.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/riding-2x.png'
          },
          { 
            :name => 'Product 3', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind-2x.png' 
          }
        ],
        :other_products => [
          { 
            :name => 'Category 1', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/handheld.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/handheld-2x.png' 
          },
          { 
            :name => 'Category 2', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers-2x.png'
          },
          { 
            :name => 'Category 3', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles-2x.png'
          }
        ]
      },
      "cub-cadet-authorized" => { 
        :name => "Cub Cadet Authorized Dealer",
        :short_name => "ccad",
        :image => "/wcpics/CubCadetMobile/en_US/images/content/cub-cadet-authorized-large.png",
        :image_hd => "/wcpics/CubCadetMobile/en_US/images/content/cub-cadet-authorized-large-2x.png",
        :products => [
          { 
            :name => 'Product 1', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/lawn-garden-2x.png' 
          },
          { 
            :name => 'Product 2', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/riding.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/riding-2x.png'
          },
          { 
            :name => 'Product 3', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/walk-behind-2x.png' 
          }
        ],
        :other_products => [
          { 
            :name => 'Product 4', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/handheld.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/handheld-2x.png' 
          },
          { 
            :name => 'Product 5', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/snow-throwers-2x.png'
          },
          { 
            :name => 'Product 6', 
            :image => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles.png', 
            :image_hd => '/wcpics/CubCadetMobile/en_US/images/content/utility-vehicles-2x.png'
          }
        ]
      }
    }
  end
  
end