class Article
  
  def self.categories
    @products = { 
                  'get-to-know' => { 'name' => 'Get to know your Cub Cadet' },
                  'maintenance' => { 'name' => 'Maintenance' },
                  'repairs' => { 'name' => 'Repairs' },
                  'safety' => { 'name' => 'Safety' },
                  'tricks-tips' => { 'name' => 'Tricks &amp; Tips' }
                }
  end
  
end  
