class Parts
  @parts = { 
    '1419369' => { 
      'name' => '42&quot;/46&quot; Double Bagger', 
      'item' => '19A70020100', 
      'photo' => 'part.png', 
      'photo_hd' => 'part-2x.png', 
      'photo_detail' => 'part-2x.png', 
      'photo_detail_hd' => 'part-2x.png', 
      'backordered' => true,
      'reg_price' => 399.00,
      'sale_price' => 359.00
    }
  } 
    
  def self.get_all()
    @parts
  end
  
  def self.get(id)
    @parts[id]
  end
end