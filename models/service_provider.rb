class ServiceProvider
  
  def self.all
    @service_providers = [
      { 
        :id => 2, 
        :name => "Worcester Sales & Svc, Inc.", 
        :address => "34200 Lorain Road", 
        :city => "North Ridgeville", 
        :state => "OH", 
        :zip => "44039", 
        :phone => "(440) 327-2196",
        :miles => 11.1,
        :hasWebsite => true
      },
      { 
        :id => 3, 
        :name => "Van Aken Hardware, Inc.", 
        :address => "20159 Van Aken Blvd.", 
        :city => "Shaker Heights", 
        :state => "OH", 
        :zip => "44122", 
        :phone => "(216) 752-2288",
        :miles => 14.1,
        :hasWebsite => false
      },
      { 
        :id => 4, 
        :name => "Sterling Farm Equipment", 
        :address => "856 Medina Road", 
        :city => "Medina", 
        :state => "OH", 
        :zip => "44256", 
        :phone => "(330) 239-9490",
        :miles => 17.1,
        :hasWebsite => false
      },
      { 
        :id => 5, 
        :name => "Four Seasons Outdoor Power Equip.", 
        :address => "30013 Euclid Avenue", 
        :city => "Wickliffe", 
        :state => "OH", 
        :zip => "44092", 
        :phone => "(440) 943-1809",
        :miles => 23.2,
        :hasWebsite => true
      }
    ]
  end
  
end