class Diagnose
  
  @categories = { 
    'push-mowers' => 'Push Mowers', 
    'self-propelled-mowers' => 'Self-Propelled Mowers', 
    'lawn-tractors' => 'Lawn Tractors', 
    'garden-tractors' => 'Garden Tractors', 
    'zero-turn-mowers' => 'Zero Turn Mowers', 
    'chippers-shredders-vacuums' => 'Chippers, Shredders &amp; Vacuums', 
    'tillers' => 'Tillers', 
    'log-splitters' => 'Log Splitters', 
    'snow-throwers' => 'Snow Throwers', 
    'handheld-products' => 'Handheld Products'
  } 

  def self.get_all_categories()
    @categories
  end
  
  def self.get_category(category)
    @categories[category]
  end
end