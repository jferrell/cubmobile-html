class ServiceLocator

  attr_accessor :index, :id, :name, :street, :city, :state, :zip, :phone, :latitude, :longitude, :store_logo, :categories, :independent, :limited, :website, :distance

  @@service_centers = {
    'cubcadet' => [
      {
        "index" => 1,
        "id" => 1,
        "website" => "#",
        "name" => "Chilton Turf Center",
        "street" => "607 Craighead St",
        "city" => "Nashville",
        "state" => "TN",
        "zip" => "37204",
        "distance" => 4.7,
        "phone" => "(615) 254-1637",
        "latitude" => 36.126364,
        "longitude" => -86.775242,
        "store_logo" => "",
        "categories" => "1,2,3,4,5,6,7,8"
      },
      {
        "index" => 2,
        "id" => 2,
        "website" => false,
        "name" => "Underwood Repair Service Inc",
        "street" => "1706 Antioch Pike",
        "city" => "Antioch",
        "state" => "TN",
        "zip" => "37013",
        "distance" => 5.2,
        "phone" => "(615) 833-2022",
        "latitude" => 36.071614,
        "longitude" => -86.683045,
        "store_logo" => "",
        "categories" => "2,3,4,5,6"
      },
      {
        "index" => 3,
        "id" => 3,
        "website" => false,
        "name" => "Fixwell Power Equipment Inc",
        "street" => "1228 Northgate Business Parkway",
        "city" => "Madison",
        "state" => "TN",
        "zip" => "37115",
        "distance" => 6.8,
        "phone" => "(615) 856-6608",
        "latitude" => 36.284116,
        "longitude" => -86.690635,
        "store_logo" => "",
        "categories" => "2,3,4,5,6"
      },
      {
        "index" => 4,
        "id" => 4,
        "website" => false,
        "name" => "J & K Small Engine Repair",
        "street" => "110 Gingham Drive",
        "city" => "La Vergne",
        "state" => "TN",
        "zip" => "37086",
        "distance" => 7.5,
        "phone" => "(615) 469-2914",
        "latitude" => 36.029822,
        "longitude" => -86.561590,
        "store_logo" => "",
        "categories" => "1,2,3,4,5,6,7,8"
      },
      {
        "index" => 5,
        "id" => 5,
        "website" => "#",
        "name" => "96 Lawn & Garden Center LLC",
        "street" => "4132 Highway 96",
        "city" => "Burns",
        "state" => "TN",
        "zip" => "37029",
        "distance" => 8.2,
        "phone" => "(615) 446-4111",
        "latitude" => 36.032124,
        "longitude" => -87.204355,
        "store_logo" => "",
        "categories" => "2,3,4,5"
      },
      {
        "index" => 6,
        "id" => 6,
        "website" => false,
        "name" => "Mikes Small Engine",
        "street" => "5333 Weakley Road",
        "city" => "Mt. Juliet",
        "state" => "TN",
        "zip" => "37122",
        "distance" => 9.7,
        "phone" => "(615) 223-5375",
        "latitude" => 36.066439,
        "longitude" => -86.489030,
        "store_logo" => "",
        "categories" => "2,3,4,5"
      }
    ]
  }

  @@categories = [
    {
      "id" => "2",
      "slug" => "category-lawn-garden",
      "name" => "Lawn &amp; Garden Tractors"
    },
    {
      "id" => "3",
      "slug" => "category-zero-turn",
      "name" => "Zero-Turn Riding Mowers"
    },
    {
      "id" => "4",
      "slug" => "category-walk-behind",
      "name" => "Walk Behind Mowers"
    },
    {
      "id" => "5",
      "slug" => "category-handheld",
      "name" => "Handheld &amp; Cleanup"
    },
    {
      "id" => "6",
      "slug" => "category-snow-thrower",
      "name" => "Snow Throwers"
    },
    {
      "id" => "7",
      "slug" => "category-utility",
      "name" => "Utility Vehicles"
    }
  ]

  def initialize(obj)
    obj.keys.each do |k|
      instance_variable_set "@#{k}", obj[k]
    end
  end

  def self.service_centers(brand)
    @@service_centers[brand].map { |s| ServiceLocator.new(s) }
  end

  def self.categories
    @@categories
  end

  def self.find(id, brand)
    service_center_collection = @@service_centers[brand].select{|service_center| service_center["id"] == id.to_i }
    ServiceLocator.new(service_center_collection[0])
  end

  def self.to_json(arry)
    locations = []
    arry.each_with_index do |s, i|
      service_center = {}
      s.instance_variables.each do |n|  
        service_center[:"#{n.to_s.sub('@','')}"] = s.instance_variable_get(n)
      end
      locations << service_center
    end
    {:locations => locations}.to_json
  end

end