require 'sinatra'
require 'json'
require 'sinatra/content_for'
require 'sinatra/cookies'
require_relative 'models/cart'
require_relative 'models/store'
require_relative 'models/service_provider'
require_relative 'models/order'
require_relative 'models/article'
require_relative 'models/parts'
require_relative 'models/products'
require_relative 'models/diagnose'
require_relative 'models/dealers'
require_relative 'models/models'
require_relative 'models/product_finder'
require_relative 'models/store_locator'
require_relative 'models/service_locator'

configure do
  enable :sessions
  set :session_secret, 'b0sco'
  disable :protection
  if ENV['RACK_ENV'] != "development"
    set :static_cache_control, [:public, :max_age => 31536000]
  end
end

before do
  @serve_minified = false
  if ENV['RACK_ENV'] != "development"
    expires 3000, :public, :must_revalidate
    @serve_minified = true
  end
  @cart = session[:cart]
  @cart_items = Cart.total_items(session[:cart])  
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
end

get '/' do
  @page = 'home'
  erb :index, :layout => :layout
end

get '/find-store/results' do
  @page = 'where-to-buy'
  if !params[:zip].nil? && params[:zip] != ""
    @default_zip = params[:zip]
  elsif !cookies[:recent_zip].nil? && cookies[:recent_zip] != ""
    @default_zip = cookies[:recent_zip]
  else
    @default_zip = 37204
  end
  @force_jquery = true
  @zip_options = [params[:zip], cookies[:recent_zip], "37204"].uniq
  @stores = StoreLocator::stores('cubcadet')
  @categories = StoreLocator::categories
  erb :find_store, :layout => :layout
end


get '/find-service-center' do
  @page = 'where-to-buy'
  if !params[:zip].nil? && params[:zip] != ""
    @default_zip = params[:zip]
  elsif !cookies[:recent_zip].nil? && cookies[:recent_zip] != ""
    @default_zip = cookies[:recent_zip]
  else
    @default_zip = 37204
  end
  @force_jquery = true
  @zip_options = [params[:zip], cookies[:recent_zip], "37204"].uniq
  @service_centers = ServiceLocator::service_centers('cubcadet')
  @categories = ServiceLocator::categories
  erb :find_service_center, :layout => :layout
end

get '/find-service-center/results' do
  @page = 'where-to-buy'
  if !params[:zip].nil? && params[:zip] != ""
    @default_zip = params[:zip]
  elsif !cookies[:recent_zip].nil? && cookies[:recent_zip] != ""
    @default_zip = cookies[:recent_zip]
  else
    @default_zip = 37204
  end
  if !params[:product_type].nil? && params[:product_type] != ""
    @default_product_type = params[:product_type]
  else
    @default_product_type = false
  end
  @force_jquery = true
  @zip_options = [params[:zip], cookies[:recent_zip], "37204"].uniq
  @service_centers = ServiceLocator::service_centers('cubcadet')
  @categories = ServiceLocator::categories
  erb :find_service_center_results, :layout => :layout
end

get '/products' do
  @page = 'products'
  erb :products_browse, :layout => :layout
end

get '/products/finder' do
  @page = 'products'
  erb :products_finder, :layout => :layout
end

get '/products/finder/results' do
  @page = 'products'
  erb :products_finder_results, :layout => :layout
end

get '/products/finder/:category' do
  content_type :json
  @content = ProductFinder.get(params[:category])
  @content.to_json
end

get '/owners/register' do
  @page = 'products'
  erb :products_register, :layout => :layout
end

post '/owners/register' do
  @page = 'products'
  erb :products_register_thanks, :layout => :layout
end

get '/products/detail/:id' do
  @page = 'products'
  @id = params[:id]
  @product = Product.get(@id)
  erb :products_detail, :layout => :layout
end

get '/products/:category/:series' do
  @page = 'products'
  @category = params[:category]
  @category_title = Product.get_category(params[:category])
  @series = params[:series]
  @products = Product.get_all();
  erb :products_list, :layout => :layout
end

get '/products/:category' do
  @page = 'products'
  @category = params[:category]
  @category_title = Product.get_category(params[:category])
  @products = Product.get_all();
  erb :products_browse, :layout => :layout
end

get '/stores/:store' do
  @page = 'stores'
  @store_slug = params[:store]
  @store = Store.stores[@store_slug]
  erb :stores, :layout => :layout
end

get '/stores/:store/:category' do
  @page = 'stores'
  @store_slug = params[:store]
  @store = Store.stores[@store_slug]
  erb :stores_category, :layout => :layout
end

get '/owners' do
  @page = 'owners'
  erb :owners, :layout => :layout
end

get '/finance' do
  @page = 'finance'
  erb :finance, :layout => :layout
end

get '/promotion' do
  @page = 'promotion'
  erb :promotion, :layout => :layout
end

get '/contact' do
  @page = 'contact'
  erb :contact, :layout => :layout
end

get '/find-parts' do
  @page = 'find-parts'
  erb :find_parts, :layout => :layout
end

post '/find-parts' do
  random = rand(9)
  if random == 0
    @page = 'find-parts'
    if !params[:part_number].nil?
      @part_not_found = true
    end
    erb :find_parts, :layout => :layout
  else  
    redirect '/find-parts/1419369'
  end
end

get '/find-parts/:id' do
  @page = 'find-parts'
  @id = params[:id]
  @product = Parts.get(@id)
  erb :find_parts_results, :layout => :layout
end

get '/manuals' do
  @page = 'manuals'
  erb :manuals, :layout => :layout
end

get '/manuals/results' do
  @page = 'manuals'
  erb :manuals_results, :layout => :layout
end

get '/manuals/:id' do
  @page = 'manuals'
  erb :manuals_disclaimer, :layout => :layout
end

get '/orders' do
  if logged_in?
    redirect to '/orders/list'
  end
  @page = 'orders'
  erb :order_status, :layout => :layout
end

get '/orders/details' do
  if logged_in?
    redirect to '/orders/list'
  end
  @page = 'orders'
  random = rand(4)
  if random == 0
    @page_error = true
    erb :order_status, :layout => :layout
  else
    erb :order_details, :layout => :layout
  end
end

get '/orders/view-details' do
  @page = 'orders'
  if params[:order_number].nil?
    @active_order = Order.get_order(0)
  else
    @active_order = Order.get_order(params[:order_number])
  end
  erb :order_view_details, :layout => :layout
end

get '/orders/list' do
  if !logged_in?
    redirect to '/orders'
  end
  @page = 'orders'
  @order_numbers = Order.orders
  erb :order_status_results, :layout => :layout
end

get '/orders/tracking' do
  if params[:order_number].nil?
    redirect to '/orders/list'
  end
  @page = 'orders'
  erb :order_tracking, :layout => :layout
end

get '/locate-model' do
  @page = 'locate-model'
  erb :locate_model, :layout => :layout
end

get '/search' do
  @page = 'search'
  random = rand(3)
  if random != 0
    erb :search_results, :layout => :layout   
  else
    erb :search_results, :layout => :layout
  end
end

get '/knowledge-center/:category' do
  @page = 'knowledge-center'
  @category = Article.categories[params[:category]]
  erb :knowledge_center_category, :layout => :layout  
end

get '/knowledge-center/:category/article-step-by-step' do
  @page = 'knowledge-center'
  erb :knowledge_center_article_steps, :layout => :layout  
end

get '/knowledge-center/:category/video-step-by-step' do
  @page = 'knowledge-center'
  erb :knowledge_center_video_steps, :layout => :layout  
end

get '/knowledge-center/:category/article' do
  @page = 'knowledge-center'
  erb :knowledge_center_article, :layout => :layout  
end

get '/knowledge-center/:category/article-table' do
  @page = 'knowledge-center'
  erb :knowledge_center_article_table, :layout => :layout  
end

get '/cart' do
  @page = 'show-cart'
  erb :cart, :layout => :layout
end

get '/cart/add/error/:product_id' do
  @page = 'cart'
  @product_id = params[:product_id]
  @product = Product.get(@product_id)
  erb :cart_add_error, :layout => :layout
end

post '/cart/update' do
  @page = 'cart'
  session[:cart] = Cart.update_item(
                          session[:cart], 
                          params[:product_id],
                          params[:product_quantity]
                        )
  redirect to('/cart')
end

post '/cart/delete' do
  @page = 'cart'
  session[:cart] = Cart.delete_item(
                          session[:cart], 
                          params[:product_id]
                        )
  redirect to('/cart')
end

post '/cart' do
  random = rand(9)
  if random != 0
    @page = 'cart'
    session[:cart] = Cart.add_item(
                            session[:cart], 
                            params[:product_id],
                            params[:product_price], 
                            params[:product_quantity]
                          )
    redirect to('/cart')
  else
    redirect to ('/cart/add/error/' + params[:product_id])
  end 
end

post '/cart/promo' do
  @page = 'show-cart'
  @error = true
  erb :cart, :layout => :layout
end

get '/cart/checkout' do
  if logged_in?
    redirect to '/cart/checkout/shipping'
  end
  @page = 'cart'
  erb :checkout_signin, :layout => :layout
end

post '/cart/checkout/signin' do
  redirect to('/cart/checkout/shipping')
end

get '/cart/checkout/shipping' do
  @page = 'cart'
  erb :checkout_shipping, :layout => :layout
end

post '/cart/checkout/shipping' do
  redirect to('/cart/checkout/payment')
end

get '/cart/checkout/payment' do
  @page = 'cart'
  erb :checkout_payment, :layout => :layout
end

post '/cart/checkout/payment' do
  redirect to('/cart/checkout/thanks')
end

get '/cart/checkout/thanks' do
  @page = 'cart'
  erb :checkout_thanks, :layout => :layout
end

get '/models/:product_type' do
  content_type :json
  Model.get(params[:product_type])
end

get '/full-site' do
  @page = 'view-full-site'
  erb :full_site, :layout => :layout
end

get '/finance-apply' do
  @page = 'finance-apply'
  erb :finance_apply, :layout => :layout
end

get '/privacy' do
  @page = 'privacy'
  erb :privacy_policy, :layout => :layout
end

get '/register' do
  @page = 'register'
  erb :account_create, :layout => :layout
end

post '/register' do
  @page = 'register'
  response.set_cookie("logged-in", true)
  erb :account_create_thanks, :layout => :layout
end

get '/forgot-password' do
  @page = 'sign-in'
  @from = params[:from]
  erb :forgot_password, :layout => :layout
end

post '/forgot-password' do
  if params[:email]
    @page_message = true
  end
  @page = 'sign-in'
  erb :forgot_password, :layout => :layout
end

get '/change-password' do
  @page = 'change-password'
  erb :change_password, :layout => :layout
end

post '/change-password' do
  @page = 'change-password-success'
  erb :change_password_success, :layout => :layout
end

get '/login' do
  @page = 'sign-in'
  erb :sign_in, :layout => :layout
end

post '/login' do
  response.set_cookie("logged-in", true)
  if session[:return_to] && session[:return_to] != ''
    @return_url = session[:return_to]
    session[:return_to] = ''
    redirect to @return_url
  elsif params[:return_url] && params[:return_url] != ''
    redirect to params[:return_url]
  elsif request.referer && request.url != request.referer
    redirect to request.referer
  else
    redirect to '/'
  end
end

get '/logout' do
  request.cookies["logged-in"] = nil;
  response.set_cookie("logged-in", false)
  if request.referer && request.url != request.referer 
    redirect to request.referer
  else
    redirect to '/'
  end
end

##################
# helper methods #
##################
def logged_in?
  return request.cookies["logged-in"] == 'true'
end